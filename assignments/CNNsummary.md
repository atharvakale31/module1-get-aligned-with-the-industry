# Convolutional Neural Networks
        
-  A convolutional neural network (CNN or ConvNet) is one of the most popular algorithms for deep learning, a type of machine learning in which a model learns to perform classification tasks directly from images, video, text, or sound.
- CNNs are particularly useful for finding patterns in images to recognize objects, faces, and scenes.

### Important Features of CNN:

- CNNs eliminate the need for manual feature extraction—the features are learned directly by the CNN.
- CNNs produce state-of-the-art recognition results.
- CNNs can be retrained for new recognition tasks, enabling you to build on pre-existing networks.

![Deep learning workflow](https://in.mathworks.com/solutions/deep-learning/convolutional-neural-network/_jcr_content/mainParsys/band_copy_copy_14735/mainParsys/columns_1606542234_c/2/image.adapt.1200.high.jpg/1594751660683.jpg)

### Working of CNN:

- A convolutional neural network can have tens or hundreds of layers that each learn to detect different features of an image.
- Filters are applied to each training image at different resolutions, and the output of each convolved image is used as the input to the next layer.
- The filters can start as very simple features, such as brightness and edges, and increase in complexity to features that uniquely define the object.

### Three Common Layers of CNN are:

![Basic Layers](https://in.mathworks.com/solutions/deep-learning/convolutional-neural-network/_jcr_content/mainParsys/band_copy_copy_14735_1026954091/mainParsys/columns_1606542234_c/2/image_2128876021_cop.adapt.1200.high.svg/1594751661348.svg)

- **Convolution** puts the input images through a set of convolutional filters, each of which activates certain features from the images.
- **Rectified linear unit (ReLU)** allows for faster and more effective training by mapping negative values to zero and maintaining positive values. This is sometimes referred to as activation, because only the activated features are carried forward into the next layer.
- **Pooling** simplifies the output by performing nonlinear downsampling, reducing the number of parameters that the network needs to learn.

![CNN with many Layers](https://in.mathworks.com/solutions/deep-learning/convolutional-neural-network/_jcr_content/mainParsys/band_copy_copy_14735_1026954091/mainParsys/columns_1606542234_c/2/image.adapt.1200.high.jpg/1594751661384.jpg)

- After learning features in many layers, the architecture of a CNN shifts to classification.

- The next-to-last layer is a fully connected layer that outputs a vector of K dimensions where K is the number of classes that the network will be able to predict. This vector contains the probabilities for each class of any image being classified.

- The final layer of the CNN architecture uses a classification layer such as softmax to provide the classification output.
