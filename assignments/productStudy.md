## **Product Name #1**

ShelfWatch

**Product Link**

[Link](https://www.karna.ai/retail-shelf-monitoring)

**Product Short Description**

ShelfWatch provides Image Recognition (IR) solution to drive perfect store execution. ShelfWatch enables CPG and Retailers to get in-store insights from shelf photos that help them to deliver superior customer experience and optimize their marketing spend.

**Product is combination of features**

1. ID Recognition 
2. Object Detection
3. Image Recognition


**Product is provided by which company?**

Karna AI

## **Product Name #2**

OrCam MyEye

**Product Link**

[Link](https://www.orcam.com/en/myeye2/)

**Product Short Description**

OrCam MyEye is a revolutionary voice activated device that attaches to virtually any glasses. It can instantly read to you text from a book, smartphone screen or any other surface, recognize faces, help you shop on your own, work more efficiently, and live a more independent life!
OrCam MyEye conveys visual information audibly, in real-time and offline.

**Product is combination of features**

1. Object detection
2. Image Recognition
3. Word Recognition


**Product is provided by which company?**

 OrCam

## **Product Name #3**

wrnchAI

**Product Link**

[Link](https://wrnch.ai/product/#product_1)

**Product Short Description**

SThe wrnchAI platform helps developers enhance their applications by enabling computers to see human motion, shape, and intent.

**Product is combination of features**

1. Pose Detection
2. Object Detection
3. Image Recognition


**Product is provided by which company?**

wrnchAI

## **Product Name #4**

Sighthound ALPR

**Product Link**

[Link](https://www.sighthound.com/products/alpr)

**Product Short Description**

Detect, recognize and search vehicle license plates, make, model, and color.  Works with live cameras and pre-recorded video or images. 

**Product is combination of features**

1. Object detection
2. Image Recognition
3. Word Recognition


**Product is provided by which company?**

Sighthound
